﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SpellChecker_Firuza
{
    class Program
    {
        static void Main(string[] args)
        {
            string inputString = System.IO.File.ReadAllText(
            Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName + "\\input.txt");
          
            var cleanInput = Regex.Replace(inputString.Replace(System.Environment.NewLine, " "), @"\s+", " ");
            var wordSets = cleanInput.Trim().Split("===");
            var dictionaryWords = wordSets[0].Trim().Split(" ");
            var inputWords = wordSets[1].Trim().Split(" ");

            var output = new List<string>();
            foreach (var input in inputWords)
            {
                var corrections = new List<Correction>();
                if (dictionaryWords.Contains(input))
                {
                    corrections.Add(new Correction { correctSpelling = input, cost = -1 });
                }
                else
                {
                    foreach (var dict in dictionaryWords)
                    {
                        int cost = Compute(input, dict);
                        if (cost > 2) continue;

                        if (input.Length - dict.Length >= 2 && hasAdjuscentDeletion(input, dict)) continue;//do not allow to adjuscent deletions
                        corrections.Add( new Correction { correctSpelling = dict, cost = cost });
                    }
                }

                if (corrections.Any(x => x.cost == 1 ))
                    corrections = corrections.Where(x => x.cost == 1 && (input.Length != x.correctSpelling.Length)).ToList();

                if (corrections.Count == 0)
                    output.Add("{" + input + "?}");
                else if (corrections.Count == 1)
                    output.Add(corrections[0].correctSpelling);
                else
                    output.Add("{" + string.Join(" ", corrections.Select(x => x.correctSpelling + " ")) + "}");
            }


            Console.WriteLine(string.Join(" ", output));
        }

        private static bool hasAdjuscentDeletion(string input, string dict)
        {
            var len = Math.Max(dict.Length, input.Length);

            for (int i = 0; i < len - 1; i++)
            {
                if (i > dict.Length - 1 || i > input.Length - 1) return true;
                if (input[i] != dict[i])
                {
                    if (i +1 > dict.Length - 1  || i + 1 > input.Length - 1) return true;
                    if (input[i + 1] != dict[i + 1]) return true;
                }
            }
            return false;
        }

        class Correction
        {
            public int cost { get; set; }
            public string correctSpelling { get; set; }
        }
        static int Compute(string inputWord, string dictionaryWord)
        {
            var inputWordLength = inputWord.Length;
            var dictWordLength = dictionaryWord.Length;

            var matrix = new int[inputWordLength + 1, dictWordLength + 1];

            if (inputWordLength == 0)
                return dictWordLength;

            if (dictWordLength == 0)
                return inputWordLength;

            // Initialization of matrix with row size inputWordLength and columns size dictWordLength
            for (var i = 0; i <= inputWordLength; matrix[i, 0] = i++) { }
            for (var j = 0; j <= dictWordLength; matrix[0, j] = j++) { }


            for (var i = 1; i <= inputWordLength; i++)
            {
                for (var j = 1; j <= dictWordLength; j++)
                {
                    var prevCost1 = (i == 1) ? 0 : (dictionaryWord[j - 1] == inputWord[i - 2]) ? 0 : 1;
                    var prevCost2 = (j == 1) ? 0 : (dictionaryWord[j - 2] == inputWord[i - 1]) ? 0 : 1;
                    var prevCost3 = (i == 1 || j == 1) ? 0 : (dictionaryWord[j - 2] == inputWord[i - 2]) ? 0 : 1;
                    var prevCost4 = (i == 1 || j == 1) ? 0 : (dictionaryWord[j - 2] == inputWord[i - 2]) ? 0 : 1;

                    var cost = (dictionaryWord[j - 1] == inputWord[i - 1]) ? 0 : 1;

                    if ((cost != 0 && (prevCost1 != 0 || prevCost2 != 0 || prevCost3 != 0 || prevCost4 != 0))) cost = 100;//do not allow adjusent subst
                    matrix[i, j] = Math.Min(  
                                            Math.Min(matrix[i - 1, j] + 1, matrix[i, j - 1] + 1),
                                            matrix[i - 1, j - 1] + cost
                                            );

                }
            }

            return matrix[inputWordLength, dictWordLength];
        }
    }
}
